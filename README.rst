==========================
RPi4 System Ready Firmware
==========================
:Authors:
        Al Stone <ahs3@redhat.com>
:Version:
        2022-01-07
:Copyright:
        2022

.. raw:: html

   <embed>
   <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
   <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
   </a><br />
   This work is licensed under a
   <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
   Creative Commons Attribution-ShareAlike 4.0 International License</a>.
   </embed>

Introduction
------------
This repository contains all the components necessary to build firmware for the
`Raspberry Pi 4 <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/>`_
that is in compliance with
`Arm SystemReady <https://developer.arm.com/architectures/system-architectures/arm-systemready>`_
requirements.

In particular, this repository has been set up in order to:

* Build RPi4 firmware from the most recent sources, and
* Run the Arm SystemReady ACS (the compliance test suites)

In general, development should occur in
`edk2 upstream source trees <https://github.com/tianocore>`_ found on github.
Those trees are used as submodules here so that this repository really only
contains the glue to build and test the firmware.

Initial Setup
-------------
To get the source tree set up for building the firmware, clone this
repository and run the ``init.sh`` script; note that this only needs to be
done once, the very first time the repository is cloned. ::

   $ git clone https://gitlab.com/t5277/rpi4-system-ready.git
   $ cd rpi4-system-ready
   $ ./init.sh

This script can be run as often as you like, actually, but does run the risk
of modifying any code in the submodule directories.

Building the Firmware
---------------------

Running the Compliance Suite
----------------------------
